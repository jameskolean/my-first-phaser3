import Phaser from 'phaser'

import ParalaxScene from './scenes/parallax-scene'

const config = {
  type: Phaser.AUTO,
  width: 800,
  height: 600,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 200 },
      //      debug: true,
    },
  },
  scene: [ParalaxScene],
}

export default new Phaser.Game(config)
