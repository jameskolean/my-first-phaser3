export const createSpikeMan = (scene, x, y, platform) => {
    const enemy = scene.physics.add
      .sprite(x, y, 'sprites', 'spikeMan-stand.png')
      .setScale(0.5)
    enemy.anims.play('spikeman-stand')
  
    scene.physics.add.collider(platform, enemy)
    
    return enemy
}