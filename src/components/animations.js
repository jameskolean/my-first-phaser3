export const keys = { HURT: 'hurt', JUMP: 'jump', STAND: 'stand', WALK: 'walk', SPIKEMAN_STAND: 'spikeman-stand', SPIKEMAN_JUMP: 'spikeman-jump', SPIKEMAN_WALK: 'spikeman-walk' }
/**
 * @param {Phaser.Scene} scene
 * @param {string} atlas
 * **/
const createPlayerAnimations = (scene, atlas) => {
  const { anims } = scene
  const frameNames = anims.generateFrameNames(atlas, {
    start: 1,
    end: 2,
    prefix: 'bunny1-walk',
    suffix: '.png',
  })
  anims.create({
    key: keys.WALK,
    frames: frameNames,
    frameRate: 5,
    repeat: -1,
  })
  anims.create({
    key: keys.STAND,
    frames: [{ key: atlas, frame: 'bunny1-stand.png' }],
    frameRate: 1,
    repeat: 1,
  })
  anims.create({
    key: keys.JUMP,
    frames: [{ key: atlas, frame: 'bunny1-jump.png' }],
    frameRate: 5,
    repeat: 1,
  })
  anims.create({
    key: keys.HURT,
    frames: [{ key: atlas, frame: 'bunny1-hurt.png' }],
    frameRate: 1,
    repeat: -1,
  })
}

const createSpikeManAnimations = (scene, atlas) => {
  const { anims } = scene;
  anims.create({
    key: keys.SPIKEMAN_WALK,
    frames: anims.generateFrameNames(atlas, {
      start: 0,
      end: 1,
      prefix: 'spikeMan-walk',
      suffix: '.png',
    }),
    frameRate: 5,
    repeat: -1,
  })
  anims.create({
    key: keys.SPIKEMAN_STAND,
    frames: [{ key: atlas, frame: 'spikeMan-stand.png' }],
    frameRate: 1,
    repeat: 1,
  })
  anims.create({
    key: keys.SPIKEMAN_JUMP,
    frames: [{ key: atlas, frame: 'spikeMan-jump.png' }],
    frameRate: 5,
    repeat: 1,
  })
}

/**
 * @param {Phaser.Scene} scene
 * @param {string} atlas
 * **/
export const createAllAnimations = (scene, atlas) => {
  createPlayerAnimations(scene, atlas)
  createSpikeManAnimations(scene, atlas)
}

/**
 * @param {Phaser.Physics.Arcade.Sprite} sprite
 * @param {string} key
 * **/
export const setPlayerAnimation = (sprite, key) => {
  const { anims } = sprite
  if (anims.currentAnim.key !== key && anims.currentAnim.key !== 'jump') {
    anims.play(key)
    if (key === 'jump') anims.nextAnim = 'stand'
  }
}
