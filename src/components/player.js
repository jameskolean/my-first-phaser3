/**
 * @param {Phaser.Scene} scene
 * **/
export const createPlayer = (scene) => {
  const player = scene.physics.add
    .sprite(140, 120, 'sprites', 'bunny1-walk1.png')
    .setScale(0.5)
  player.anims.play('stand')

  scene.cameras.main.startFollow(player)
  player.body.checkCollision.up = false
  player.body.checkCollision.left = false
  player.body.checkCollision.right = false
  //  player.setCollideWorldBounds(true)
  return player
}
