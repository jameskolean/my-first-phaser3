/**
 * @param {Phaser.Scene} scene
 * @param {string} texture
 * @param {number} scrollFactor
 * **/
export const createLoopedBackground = (scene, texture, scrollFactor) => {
  const scale =
    scene.cameras.main.getBounds().height /
    scene.textures.get(texture).getSourceImage().height
  const textureWidth =
    scene.textures.get(texture).getSourceImage().width * scale
  const count = Math.ceil(scene.cameras.main.getBounds().width / textureWidth)
  let currentX = 0
  for (let i = 0; i < count; ++i) {
    const b = scene.add
      .image(currentX, scene.scale.height, texture)
      .setOrigin(0, 1)
      .setScrollFactor(scrollFactor)
    b.scale = scale
    currentX += b.width * b.scaleY
  }
}

/**
 * @param {Phaser.Scene} scene
 * @param {string} texture
 * **/
export const createStaticBackground = (scene, texture) => {
  const width = scene.scale.width
  const height = scene.scale.height

  const scaleX =
    scene.cameras.main.getBounds().height /
    scene.textures.get(texture).getSourceImage().height
  const scaleY =
    scene.cameras.main.getBounds().width /
    scene.textures.get(texture).getSourceImage().width
  //   const scale = Math.max(scaleX, scaleY)

  const b = scene.add
    .image(width * 0.5, height * 0.5, texture)
    .setScrollFactor(0)
  b.scale = Math.max(scaleX, scaleY)
  return b
}
