import Phaser from 'phaser'

export default class Spiker extends Phaser.GameObjects.Sprite {

  constructor(scene, x, y, scale, platformX, platformWidth) {
    super(scene, x, y, 'sprites', 'spikeMan-stand.png')
    
    this.setScale(scale)
    this.minX = platformX;
    this.maxX = platformX + platformWidth;
    this.direction = 1
    scene.add.existing(this)
  }

  update() {
    if (this.x < this.minX) {
      this.direction = 1      
    }
    else if (this.x > this.maxX) {
      this.direction = -1
    }
    this.x += 1 * this.direction
  }
}