/**
 * @param {Phaser.Scene} scene
 * @param {string} platform
 * @param {number} spacing
 * **/
export const createPlatforms = (scene, platform, spacing) => {
  const platforms = scene.physics.add.staticGroup()
  const platformWidth = scene.textures.get(platform).getSourceImage().width
  const platformTotalWidth = platformWidth + spacing
  const count = Math.floor(
    scene.cameras.main.getBounds().width / platformTotalWidth
  )
  let x = platformWidth / 2
  for (let i = 0; i < count; ++i) {
    const y = Phaser.Math.Between(500, 300)
    /** @type{Phaser.Physics.Arcade.Sprite} */
    const platform = platforms.create(x, y, 'platform')
    platform.scale = 0.5
    platform.body.updateFromGameObject()
    x += platformTotalWidth
  }
  return platforms
}

/**
 * @param {Phaser.Scene} scene
 * **/
export const createGround = (scene) => {
  //add ground
  const groundX = scene.cameras.main.getBounds().width / 2
  const groundY = scene.sys.game.config.height
  const ground = scene.physics.add.staticSprite(groundX, groundY, 'block')
  //ground.displayWidth = this.sys.game.config.width
  ground.body.setSize(scene.cameras.main.getBounds().width, 0)
  return ground
}
