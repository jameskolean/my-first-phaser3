import Phaser, { Game } from 'phaser'
import {
  createLoopedBackground,
  createStaticBackground,
} from '../components/background'
import { createPlatforms, createGround } from '../components/platform'
import {
  keys as animationKeys,
  createAllAnimations,
  setPlayerAnimation,
} from '../components/animations'
import { createPlayer } from '../components/player'
import Spiker from '../components/spiker'

export default class ParallaxScene extends Phaser.Scene {

  player
  spikemen = []

  constructor() {
    super('parallax-scene')
  }

  preload() {
    this.load.image('bg-layer1', 'assets/background/bg-layer1.png')
    this.load.image('bg-layer2', 'assets/background/bg-layer2.png')
    this.load.image('bg-layer3', 'assets/background/bg-layer3.png')
    this.load.image('bg-layer4', 'assets/background/bg-layer4.png')
    this.load.image('platform', 'assets/environment/ground-grass.png')
    this.load.multiatlas('sprites', 'assets/sprites.json', 'assets')
    this.load.image('spiker', 'assets/spikeMan-stand.png')

    this.cursors = this.input.keyboard.createCursorKeys()
  }

  create() {
    createAllAnimations(this, 'sprites')
    const widthScale = 10
    const width = this.scale.width
    const height = this.scale.height
    this.cameras.main.setBounds(0, 0, width * widthScale, height)
    createStaticBackground(this, 'bg-layer1')
    createLoopedBackground(this, 'bg-layer2', 0.2)
    createLoopedBackground(this, 'bg-layer3', 0.4)
    createLoopedBackground(this, 'bg-layer4', 0.6)

    const platforms = createPlatforms(this, 'platform', 30)

    this.player = createPlayer(this)
    this.physics.add.collider(platforms, this.player)

    const ground = createGround(this)
    this.physics.add.collider(this.player, ground)

    platforms.children.each(platform => {
      const x = platform.body.position.x + Phaser.Math.Between(0, platform.body.gameObject.width / platforms.scaleX.length);
      const y = platform.body.position.y - platform.body.gameObject.height;
      const spiker = new Spiker(this, x, y, 0.4, platform.body.position.x, platform.body.gameObject.width / 2)
      this.physics.add.existing(spiker)
      this.physics.add.collider(platforms, spiker)
      this.physics.add.overlap( this.player, spiker, this.handleSkewered, undefined, this)
      this.spikemen.push(spiker)
    });
  }

  handleSkewered(player, enemy) {
    player.body.position.x = 140
    player.body.position.y = 120
  }

  update() {
    // const cam = this.cameras.main
    const speed = 3
    // if (this.cursors.left.isDown) {
    //   cam.scrollX -= speed
    // } else if (this.cursors.right.isDown) {
    //   cam.scrollX += speed
    // }
    const { player } = this
    if (this.cursors.left.isDown) {
      player.x -= speed
      setPlayerAnimation(player, animationKeys.WALK)
    } else if (this.cursors.right.isDown) {
      player.x += speed
      setPlayerAnimation(player, animationKeys.WALK)
    } else if (this.cursors.up.isDown && this.player.body.touching.down) {
      player.setVelocityY(-200)
      setPlayerAnimation(player, animationKeys.JUMP)
    } else {
      setPlayerAnimation(player, animationKeys.STAND)
    }

    this.spikemen.forEach(spiker => spiker.update())

    // add bounce
    // const touchingDown = this.player.body.touching.down
    // if (touchingDown) {
    //   player.setVelocityY(-20)
    // }
  }
}
