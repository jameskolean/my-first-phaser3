# Notes on building this Game

- Background assest came from https://itch.io specifically https://digitalmoons.itch.io/parallax-forest-background
- Project template came from https://github.com/ourcade/phaser3-parcel-template

  ```bash
  git clone git@github.com:photonstorm/phaser3-project-template.git first-phaser-project
  cd first-phaser-project
  npm install
  npm start
  ```

- Sprite assest came from https://itch.io specifically https://the-baldur.itch.io/pixelart-hiker
- Gitlab Repo is https://gitlab.com/jameskolean/my-first-phaser3
- Public server is https://app.netlify.com/sites/my-first-phaser3/overview
- Public URL is https://my-first-phaser3.netlify.app/
